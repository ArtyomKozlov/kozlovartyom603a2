import Generator.CachedGenerator;
import Generator.Generator;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CachedGeneratorTest {
    private static int count = 0;

    @Test
    public void cachedTest() {
        Generator generator = new CachedGenerator<Long>(
                x -> {
                    count++;
                    return (Long) x[0] + (Long) x[1];
                },1L, 1L
        );

        int c = count;

        assertEquals(generator.get(50), 12586269025L);
        assertEquals(48, count - c);
        c = count;
        assertEquals(generator.get(45), 1134903170L);
        assertEquals(0, count - c);
        c = count;
        assertEquals(generator.get(55), 139583862445L);
        assertEquals(5, count - c);
    }
}
