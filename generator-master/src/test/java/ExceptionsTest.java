import Generator.CachedGenerator;
import Generator.Generator;
import org.junit.Test;

public class ExceptionsTest {

    @Test(expected = IndexOutOfBoundsException.class)
    public void testException() {
        Generator generator = new CachedGenerator<Integer>(
                x -> (Integer) x[0],1
        );
        generator.get(0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFewArgs() {
        new Generator<Integer>(
                x -> (Integer) x[1],1
        );
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNegativeIndex() {
        new Generator<Integer>(
                x -> (Integer) x[-1],1
        );
    }
}
