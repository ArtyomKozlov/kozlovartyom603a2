import Generator.Generator;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class InitialLengthTest {

    @Test
    public void testOne() {
        Generator g = new Generator<Integer>(x -> (Integer)x[0] + 1, 1);
        assertEquals(100, g.get(100));
    }

    @Test
    public void testThree() {
        Generator g = new Generator<Integer>(x -> (Integer)x[1] + (Integer)x[2] - (Integer)x[0], 1, 1, 2);
        assertEquals(50, g.get(100));
    }
}
