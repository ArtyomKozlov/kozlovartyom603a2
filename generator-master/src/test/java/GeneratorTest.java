import Generator.Generator;
import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.assertEquals;

public class GeneratorTest {
    @Test
    public void simpleFibTest() {
        Generator fibGenerator = new Generator<BigInteger>(
                x -> ((BigInteger) x[0]).add((BigInteger) x[1]),
                new BigInteger("1"), new BigInteger("1")
        );

        BigInteger fib100 = new BigInteger("354224848179261915075");
        assertEquals(fib100, fibGenerator.get(100));
    }

    @Test
    public void hugeFibTest() {
        Generator fibGenerator = new Generator<BigInteger>(
                x -> ((BigInteger) x[0]).add((BigInteger) x[1]),
                new BigInteger("1"), new BigInteger("1")
        );

        // Don't touch this string!!!
        BigInteger fib4782 = new BigInteger(
                "1070066266382758936764980584457396885083683896632151665013235203375314520604694040621889147582489792" +
                "6578046948881775919574843364666725699595129960304612627480924821861440694330512347744427502737817530" +
                "8757939166619214925918675955396642283714894311307469950343954700198543260972306729019287052644724372" +
                "6117715821825548491120525013201478612965931381792235559657452039506137551467837543229119602129934048" +
                "2607061753977068470682028954869026661854351245219003694806413574474709117076197669456910700980243934" +
                "3961747410373691250323136553216477369702316775505159517351846057995491941096777837322966579658164651" +
                "3903488154256310184224190259846088000110186255550245493937113651657039447629584714548523425950428582" +
                "4253060835444354282126110089928637950480068943303097732178348645431132057656598684562886168087186938" +
                "3529735064398629764066000072356291790520705116407761481249188583094594056668833910935094445657635766" +
                "6151619317753792891661581327159616877487983821820492520348473874384736771934512787029218636250627816"
        );
        assertEquals(fib4782, fibGenerator.get(4782));
    }

    @Test
    public void stringTest() {
        Generator stringGenerator = new Generator<String>(
                x -> x[0].toString() + x[1],
                "0", "1");
        String expectedString = "1010110101101101011010110110101101101011010110110101101";
        assertEquals(expectedString, stringGenerator.get(10));
    }

    @Test
    public void nullTest() {
        Generator g = new Generator<Integer>(x -> null, 1, 2, 3);
        assertEquals(null, g.get(100));
    }
}
