import Generator.CachedGenerator;
import Generator.Generator;
import Generator.SieveCachedGenerator;

/**
 * Примеры использования
 */
public class App {
    public static void main(String[] args) {
        Generator g1 = new Generator<Double>(x -> (Double) x[0] + (Double) x[1], 1., 1.);
        System.out.println(g1.get(20));
        System.out.println(g1.get(50));
        System.out.println(g1.get(5));
        System.out.println(g1.get(-10));
        Generator g7 = new Generator<Integer>(x -> (Integer) x[1], 1);
        System.out.println(g7.get(10));
        System.out.println();
        Generator g2 = new CachedGenerator<Double>(x -> (Double) x[0] + (Double) x[1], 1., 1.);
        System.out.println(g2.get(20));
        System.out.println(g2.get(10));
        System.out.println(g2.get(50));
        g2.get(-123);
        System.out.println();
        Generator g3 = new SieveCachedGenerator<Double>(0.2f, x -> (Double) x[0] + (Double) x[1], 1., 1.);
        System.out.println(g3.get(20));
        System.out.println(g3.get(10));
        System.out.println(g3.get(50) + "\n\n");
        Generator g4 = new SieveCachedGenerator<String>(0.2f, x -> x[0].toString() + x[1], "1", "2");
        System.out.println(g4.get(5));
        System.out.println("\n");
        Generator g5 = new SieveCachedGenerator<Double>(0.4f, x -> (Double) x[0] + (Double) x[1], 1., 1.);
        System.out.println(g5.get(20));
        System.out.println(g5.get(50));
        System.out.println(g5.get(5));
        Generator g6 = new SieveCachedGenerator<Double>(0.6f, x -> (Double) x[0] + (Double) x[1], 1., 1.);
        System.out.println(g6.get(20));
        System.out.println(g6.get(50));
        System.out.println(g6.get(5));
    }
}
