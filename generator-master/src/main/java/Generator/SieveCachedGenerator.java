package Generator;

import java.util.Arrays;

public class SieveCachedGenerator<T> extends CachedGenerator<T>{
    float proc;
    /**
     * Создает генератор с частичным заполнением
     *
     * @param filling плотность заполнения кэша в процентах
     * @param rule функция, вычисляющая очередной член последовательности
     * @param starts начальные члены последовательности
     * @throws IllegalArgumentException если задано недопустимое заполнение или недопустимая формула
     */
    @SafeVarargs
    public SieveCachedGenerator(float filling, Recurrent<T> rule, T... starts) throws IllegalArgumentException {
        this(rule, starts);
        proc = filling;
    }

    /**
     * Создает генератор с полным заполнением
     *
     * @param rule функция, вычисляющая очередной член последовательности
     * @param starts начальные члены последовательности
     * @throws IllegalArgumentException если задана недопустимая формула
     */
    @SafeVarargs
    public SieveCachedGenerator(Recurrent<T> rule, T... starts) throws IllegalArgumentException {
        super(rule, starts);
        proc = -1;
    }

    @Override
    public T get(int n) throws IndexOutOfBoundsException {
        try {
            if (n < 1) {
                throw new IndexOutOfBoundsException("Порядковый номер числа меньше еденицы. " +
                        "\nSieveCachedGenerator -> get -> Число " + n + " посланое в метод меньше 1.");
            }
        } catch (IndexOutOfBoundsException e) {
            System.out.println("\nWarning! Exception: ");
            System.out.println("recaught: " + e + "\nВычислено число с порядковым номером 1.");
            n = 1;
        }
        T cash3;
        int cyclicality = (int) (20 / (proc * 10));
        int req_e = (((n - 1) / cyclicality) * 2) + 2;
        if (req_e > startLength) {
            int numberRep = cyclicality - 2;
            while (req_e > startLength) {
                startValues = Arrays.copyOf(startValues, (startLength + 2));
                startValues[startLength] = rule.next(startValues[startLength - 2], startValues[startLength - 1]);
                startValues[startLength + 1] = rule.next(startValues[startLength - 1], startValues[startLength]);
                //System.out.println(startValues[startLength] + " " + startValues[startLength + 1] + " ");
                T cash;
                for (int i = 0; i < numberRep; ++i) {
                    cash = startValues[startLength + 1];
                    startValues[startLength + 1] = rule.next(startValues[startLength], startValues[startLength + 1]);
                    //System.out.println(startValues[startLength + 1] + " ");
                    startValues[startLength] = cash;
                }
                startLength = startValues.length;
            }
        }
        int k = (n - 2) % cyclicality;
        int exk = cyclicality - 1;
        if ((k == -1) && (k == exk)) {
            cash3 = startValues[req_e - 2];
        } else if (k == 0) {
            cash3 = startValues[req_e - 1];
        } else {
            T cash1;
            T cash2 = startValues[req_e - 2];
            cash3 = startValues[req_e - 1];
            for (int i = 0; i < k; ++i) {
                cash1 = cash3;
                cash3 = rule.next(cash2, cash3);
                cash2 = cash1;
            }
        }
        return cash3;
    }

}
