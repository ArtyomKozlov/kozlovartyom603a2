package Generator;

public class Generator<T> extends AbstractGenerator<T> {
    /**
     * Создает генератор
     *
     * @param rule   функция, вычисляющая очередной член последовательности
     * @param starts начальные члены последовательности
     * @throws IllegalArgumentException если задана недопустимая формула
     */
    @SafeVarargs
    public Generator(Recurrent<T> rule, T... starts) throws IllegalArgumentException {
        super(rule, starts);
    }

    @Override
    public T get(int n) throws IndexOutOfBoundsException {
        try {
            if (n < 1) {
                throw new IndexOutOfBoundsException("Порядковый номер числа меньше еденицы. " +
                        "\nGenerator -> get -> Число " + n + " посланое в метод меньше 1.");
            }
        } catch (IndexOutOfBoundsException e) {
            System.out.println("\nWarning! Exception: ");
            System.out.println("recaught: " + e + "\nВычислено число с порядковым номером 1.");
            n = 1;
        }
        if (startLength == 2) {
            T k;
            T k1 = startValues[0];
            T k2 = startValues[1];
            for (int i = 0; i < (n - 2); ++i) {
                k = startValues[1];
                startValues[1] = rule.next(startValues[0], startValues[1]);
                startValues[0] = k;
            }
            k = startValues[1];
            startValues[0] = k1;
            startValues[1] = k2;
            return k;
        } else if (startLength == 3) {
            T k, k0;
            T k1 = startValues[0];
            T k2 = startValues[1];
            T k3 = startValues[2];
            for (int i = 0; i < (n - 3); ++i) {
                k = startValues[2];
                k0 = startValues[1];
                startValues[2] = rule.next(startValues[0], startValues[1], startValues[2]);
                startValues[1] = k;
                startValues[0] = k0;
            }
            k = startValues[2];
            startValues[0] = k1;
            startValues[1] = k2;
            startValues[2] = k3;
            return k;
        }
        for (int i = 0; i < (n - 1); ++i) {
            startValues[0] = rule.next(startValues[0], 1);
        }
        return startValues[0];
    }
}
