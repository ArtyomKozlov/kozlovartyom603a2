package Generator;

import java.util.Arrays;

public class CachedGenerator<T> extends Generator<T> {

    /**
     * Создает генератор с полным заполнением
     *
     * @param rule функция, вычисляющая очередной член последовательности
     * @param starts начальные члены последовательности
     * @throws IllegalArgumentException если задана недопустимая формула
     */
    @SafeVarargs
    public CachedGenerator(Recurrent<T> rule, T... starts) throws IllegalArgumentException {
        super(rule, starts);
    }

    @Override
    public T get(int n) throws IndexOutOfBoundsException {
        try {
            if (n < 1) {
                throw new IndexOutOfBoundsException("Порядковый номер числа меньше еденицы. " +
                        "\nCachedGenerator -> get -> Число " + n + " посланое в метод меньше 1.");
            }
        } catch (IndexOutOfBoundsException e) {
            System.out.println("\nWarning! Exception: ");
            System.out.println("recaught: " + e + "\nВычислено число с порядковым номером 1.");
            n = 1;
        }
        if (startLength < n) {
            startValues = Arrays.copyOf(startValues, n);
            for (int i = startLength; i < n; ++i) {
                startValues[i] = rule.next(startValues[i - 2], startValues[i - 1]);
            }
            startLength = startValues.length;
            return startValues[n - 1];
        }
        return startValues[n - 1];
    }
}
