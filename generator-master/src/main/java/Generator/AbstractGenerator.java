package Generator;

import java.util.Arrays;

/**
 * Генератор, позволяющие получать
 * произвольный член произвольной последовательности,
 * заданной рекуррентной формулой
 */
public abstract class AbstractGenerator<T> {
    T[] startValues;
    int startLength;
    Recurrent<T> rule;

    /**
     * Создает генератор с заданной рекуррентной формулой и начальными значениями
     *
     * @param rule функция, вычисляющая очередной член последовательности
     * @param starts начальные члены последовательности
     * @throws IllegalArgumentException если задана недопустимая формула
     */
    @SafeVarargs
    AbstractGenerator(Recurrent<T> rule, T... starts) throws IllegalArgumentException {
        try {
            try {
                this.startValues = starts;
                this.startLength = starts.length;
                this.rule = rule;
                if (startLength == 1) {
                    T p = rule.next(starts[0]);
                } else if (startLength == 2) {
                    T p = rule.next(starts[0], starts[1]);
                } else if (startLength == 3) {
                    T p = rule.next(starts[0], starts[1], starts[2]);
                }
            } catch (IndexOutOfBoundsException e) {
                startValues = Arrays.copyOf(startValues, (startLength + 1));
                startValues[1] = startValues[0];
                throw new IllegalArgumentException("Задана неверная рекуррентная формула. Недостаточно начальных значений.");
            }
        }  catch (IllegalArgumentException e) {
            System.out.println("\nWarning! Exception:\nrecaught: " + e + "\nВ качестве недостающих значений взято 1 начальное.");
        }
    }

    /**
     * Возвращает произвольный член последовательности
     *
     * @param n номер члена последовательности, n > 0
     * @return заданный член последовательности
     * @throws IndexOutOfBoundsException если n < 1
     */
    public abstract T get(int n) throws IndexOutOfBoundsException;
}
